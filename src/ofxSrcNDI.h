#pragma once
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxNDI.h" // NDI classes
#include "ofxDropdown.h"
#include <future>
#include <chrono>


class ofxSrcNDI{
public:
    ofxSrcNDI();
    ~ofxSrcNDI();
    
    ofxGuiGroup gui;

    ofParameter <bool> enable;
    ofParameter <float> opacity;
    ofParameter <string> NDI_name;

    ofxLabel fpsLabel;
    ofxLabel resolutionLabel;

    void setup(string name);
    void NDI_name_changed(string &s_NDI_name);
    void update();
    void refresh_sources();
    void draw(float x, float y);
    void draw(float x, float y, float w, float h);

    ofxNDIreceiver ndiReceiver; // NDI receiver
    ofTexture ndiTexture; // Texture to receive

    unsigned char *ndiChars; // unsigned char image array to receive
	unsigned int senderWidth = 0; // sender width and height needed to receive char pixels
	unsigned int senderHeight = 0;
	void ShowInfo();

    ofParameter<string> options;
    ofxDropdown_<string> strDropdown { "NDI sources" };

    void strDropdown_changed(string & selection);

    ofxButton refreshButton;
    void onRefreshButtonPressed();

private:
    string s_loaded_NDI_src;
};