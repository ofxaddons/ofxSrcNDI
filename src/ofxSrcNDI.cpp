#include "ofxSrcNDI.h"

ofxSrcNDI::ofxSrcNDI() {}

ofxSrcNDI::~ofxSrcNDI() 
{
    refreshButton.removeListener(this, &ofxSrcNDI::onRefreshButtonPressed);
}

void ofxSrcNDI::setup(string name) 
{
    gui.setup(name);
    gui.add(enable.set("enable",1));
    gui.add(opacity.set("opacity", 1, 0,1));
    gui.add(refreshButton.setup("Refresh"));
    refreshButton.addListener(this, &ofxSrcNDI::onRefreshButtonPressed);
    gui.add(NDI_name.set("NDI_name", "hello"));
    NDI_name.addListener(this, &ofxSrcNDI::NDI_name_changed);
    refresh_sources();
    strDropdown.disableMultipleSelection(1);
    gui.add(&strDropdown);
    strDropdown.addListener(this, &ofxSrcNDI::strDropdown_changed);

    gui.add(resolutionLabel.setup("Resolution: ", "0x0")); // Initial value "0x0", but it'll get updated
    gui.add(fpsLabel.setup("FPS: ", "0.00"));           // Initial value "0.00", but it'll get updated


	cout << ndiReceiver.GetNDIversion() << " (https://www.ndi.tv/)" << endl;
    ndiChars = new unsigned char[senderWidth*senderHeight * 4];
	// Sender dimensions and fps are not known yet
	senderWidth = (unsigned char)ofGetWidth();
	senderHeight = (unsigned char)ofGetHeight();
    ndiTexture.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
    std::async(std::launch::async, [this]()
    {
         /* lamda function to get the source after a delay, else is not populate */
        std::this_thread::sleep_for(std::chrono::milliseconds(1000)); 
        this->refresh_sources();
    });
}



void ofxSrcNDI::update() 
{
    if(enable)
    {
    ndiReceiver.ReceiveImage(ndiTexture);
        fpsLabel =ofToString(ndiReceiver.GetSenderFps(), 2);  // 2 decimal places
        resolutionLabel = ofToString(ndiReceiver.GetSenderWidth()) + "x" + ofToString(ndiReceiver.GetSenderHeight());
    }
}

void ofxSrcNDI::draw(float x, float y) 
{
    if(enable)
    {
        ofSetColor(255,255,255, opacity*255);
        ndiTexture.draw(x,y);
    }
}

void ofxSrcNDI::draw(float x, float y, float w, float h) 
{
    if(enable)
    {
        ofSetColor(255,255,255, opacity*255);
	    ndiTexture.draw(x, y, w, h);
    }
}


void ofxSrcNDI::refresh_sources() {
    char name[256];

    int nsenders = ndiReceiver.FindSenders();
    cout << "nsender " << nsenders << endl;

    
    strDropdown.clear(); // Clear the dropdown before populating

    if (nsenders > 0) {
        cout << "Number of NDI senders found: " << nsenders << endl;
        for (int i = 0; i < nsenders; i++) {
            ndiReceiver.GetSenderName(name, 256, i);
            cout << "    Sender " << i << " [" << name << "]" << endl;
            strDropdown.add(name); // Add the sender name to the dropdown
        }
    } else {
        cout << "No NDI senders found" << endl;
    }
}


void ofxSrcNDI::strDropdown_changed(string &selection)
{
    std::cout << "Selected NDI source: " << selection << std::endl;
    //ndiReceiver.SetSenderName(selection.c_str());
    NDI_name=selection.c_str();
}

void ofxSrcNDI::NDI_name_changed(string &s_NDI_name)
{
    ndiReceiver.SetSenderName(s_NDI_name);
}

void ofxSrcNDI::onRefreshButtonPressed()
{
    refresh_sources();
}

